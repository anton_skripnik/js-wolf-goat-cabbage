(function(window, document) {
  const defs = {
    position: {
      left: {
        cabbage: { x: 4, y: 219, z: 3 },
        farmer: { x: 52, y: 98, z: 4 },
        goat: { x: 23, y: 180, z: 2 },
        wolf: { x: 4, y: 93, z: 1 }
      },
      right: {
        cabbage: { x: 503, y: 364, z: 3 },
        farmer: { x: 267, y: 105, z: 4 },
        goat: { x: 374, y: 352, z: 2 },
        wolf: { x: 479, y: 183, z: 1 }
      }
    },

    background: {
      imageURL: "img/background.jpg",
      width: 573,
      height: 454
    },

    bank: {
      left: 0,
      right: 1
    }
  }

  function initBackground(background) {
    background.style = `position: relative; background-image: url(${defs.background.imageURL}); width: ${defs.background.width}px; height: ${defs.background.height}px`
  }

  function updateUI(farmer) {
    function position(img, position) {
      img.style = `position: absolute; left: ${position.x}px; top: ${position.y}px; z-index: ${position.z};`
    }

    position(
      farmer.cabbage,
      (
        farmer.state.cabbage == defs.bank.left ? 
          defs.position.left.cabbage :
          defs.position.right.cabbage
      ) 
    )
    position(
      farmer.goat,
      (
        farmer.state.goat == defs.bank.left ? 
          defs.position.left.goat :
          defs.position.right.goat
      ) 
    )
    position(
      farmer.wolf,
      (
        farmer.state.wolf == defs.bank.left ? 
          defs.position.left.wolf :
          defs.position.right.wolf
      ) 
    )
    position(
      farmer.farmer,
      (
        farmer.state.farmer == defs.bank.left ? 
          defs.position.left.farmer :
          defs.position.right.farmer
      ) 
    )
  }

  function flip(bank) {
    if (bank == defs.bank.right) {
      return defs.bank.left
    } else {
      return defs.bank.right
    }
  }

  function checkResult(farmer) {
    if (farmer.state.farmer == defs.bank.right
    &&  farmer.state.cabbage == defs.bank.right
    &&  farmer.state.goat == defs.bank.right
    &&  farmer.state.wolf == defs.bank.right) {
      farmer.changeStatus("ПОБЕДА!!!!!")
      return
    }

    if (farmer.state.cabbage == farmer.state.goat
    &&  farmer.state.goat != farmer.state.farmer) {
      farmer.changeStatus("Без присмотра коза съела капусту =(")
      return
    }

    if (farmer.state.goat == farmer.state.wolf
    &&  farmer.state.wolf != farmer.state.farmer) {
      farmer.changeStatus("Без присмотра волк съел козу =(")
      return
    }
  }

  function Farmer(background, cabbage, goat, wolf, farmer, statusDiv) {
    this.cabbage = cabbage
    this.goat = goat
    this.wolf = wolf
    this.farmer = farmer

    this.state = {
      cabbage: defs.bank.left,
      farmer: defs.bank.left,
      goat: defs.bank.left,
      wolf: defs.bank.left
    }

    this.changeStatus = function(newStatus) {
      statusDiv.innerHTML = newStatus
    }

    this.transfer = function(who) {
      if (who == this.cabbage && this.state.cabbage == this.state.farmer) {
        this.state.cabbage = flip(this.state.cabbage)
      }

      if (who == this.goat && this.state.goat == this.state.farmer) {
        this.state.goat = flip(this.state.goat)
      }

      if (who == this.wolf && this.state.wolf == this.state.farmer) {
        this.state.wolf = flip(this.state.wolf)
      }

      this.state.farmer = flip(this.state.farmer)

      updateUI(this)

      checkResult(this)
    }

    initBackground(background)

    updateUI(this)
  }

  window.Farmer = Farmer
})(window, document)
